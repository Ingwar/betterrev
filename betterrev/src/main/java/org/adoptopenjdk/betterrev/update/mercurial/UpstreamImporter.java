package org.adoptopenjdk.betterrev.update.mercurial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.UntypedActor;

/**
 * Constantly polls OpenJDK repositories in order to pull in changes.
 */
public class UpstreamImporter extends UntypedActor {

    private final static Logger LOGGER = LoggerFactory.getLogger(UpstreamImporter.class);
    
    @Override
    public void onReceive(Object message) throws Exception {
        LOGGER.info("Polling openjdk for changes");
        // TODO: #31
    }

}
