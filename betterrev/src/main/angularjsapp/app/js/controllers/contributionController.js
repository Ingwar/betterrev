betterrevApp.controller('contributionController', ['$scope', '$routeParams', 'contributionsService',

    function ($scope, $routeParams, contributionsService) {

        $scope.contribution = contributionsService.get({
            contributionId: $routeParams.contributionId
        });
    }]
);